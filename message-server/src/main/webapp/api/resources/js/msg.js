var config = {
		address : function() {
			return $('#apiHost').val();
		},
		clientId : function() {
			var clientId = $('#clientId').val();
			return clientId != '' ? clientId : '196846682';
		},
		sercret : function() {
			var sercret = $('#sercret').val();
			return sercret != '' ? sercret : '4e07a39dfc7edb7w3d66f2fe4s3911e2';
		},
		timestamp : function() {
			return new Date().getTime();
		},
		sign: function(timestamp) {
			return $.md5(config.clientId() + timestamp + config.sercret());
		},
		params : function() {
			var params = {};
			params['clientId'] = config.clientId();
			params['time'] = config.timestamp();
			params['sign'] = config.sign(params.time);
			return params;
		}
};

$(function() {
	$('#commonPanel').append(['<p>API地址：<input type="text" id="apiHost" value="http://127.0.0.1:6070"></p>',
	                          '<p>clientId：<input type="text" id="clientId" value="196846682"></p>',
	                          '<p>sercret：<input type="text" id="sercret" value="4e07a39dfc7edb7w3d66f2fe4s3911e2"></p>'].join(''));
});

var api = {
};

api.sysInfo = {
		saveOrUpdate : function(sysNo, name, callback) {
			var _data = config.params();
			_data['sysNo'] = sysNo;
			_data['name'] = name;
			jQuery.ajax({
				url : config.address() + '/sysInfo/saveOrUpdate',
				data : _data,
				type : 'post',
				dataType : 'json',
				error : function(json){
					alert('异常');
				},
				success : function(json) {
					callback(json);
				}
			});
		}
};

api.userInfo = {
		saveOrUpdate : function(sysNo, userId, phone, email, callback) {
			var _data = config.params();
			_data['sysNo'] = sysNo;
			_data['userId'] = userId;
			_data['phone'] = phone;
			_data['email'] = email;
			jQuery.ajax({
				url : config.address() + '/userInfo/saveOrUpdate',
				data : _data,
				type : 'post',
				dataType : 'json',
				error : function(json){
					alert('异常');
				},
				success : function(json) {
					callback(json);
				}
			});
		}
};

api.msgGroup = {
		saveOrUpdate : function(id, sysNo, name, type, pid, callback) {
			var _data = config.params();
			_data['id'] = id;
			_data['sysNo'] = sysNo;
			_data['name'] = name;
			_data['type'] = type;
			_data['pid'] = pid;
			jQuery.ajax({
				url : config.address() + '/msgGroup/saveOrUpdate',
				data : _data,
				type : 'post',
				dataType : 'json',
				error : function(json){
					alert('异常');
				},
				success : function(json) {
					callback(json);
				}
			});
		},
		pageQuery : function(page, size, sysNo, pid, callback) {
			var _data = config.params();
			_data['page'] = page;
			_data['size'] = size;
			_data['sysNo'] = sysNo;
			_data['pid'] = pid;
			jQuery.ajax({
				url : config.address() + '/msgGroup/pageQuery',
				data : _data,
				type : 'post',
				dataType : 'json',
				error : function(json){
					alert('异常');
				},
				success : function(json) {
					callback(json);
				}
			});
		}
};

api.msgInfo = {
		save : function(sysNo, groupId, type, title, content,
				ext1, ext2, ext3, sendUserId, receUserIds,
				receContent, receEmailFiles, recePhones, receEmails, callback) {
			var _data = config.params();
			_data['sysNo'] = sysNo;
			_data['groupId'] = groupId;
			_data['type'] = type;
			_data['title'] = title;
			_data['content'] = content;
			_data['ext1'] = ext1;
			_data['ext2'] = ext2;
			_data['ext3'] = ext3;
			_data['sendUserId'] = sendUserId;
			_data['receUserIds'] = receUserIds;
			_data['receContent'] = receContent;
			_data['receEmailFiles'] = receEmailFiles;
			_data['recePhones'] = recePhones;
			_data['receEmails'] = receEmails;
			jQuery.ajax({
				url : config.address() + '/msgInfo/save',
				data : _data,
				type : 'post',
				dataType : 'json',
				error : function(json){
					alert('异常');
				},
				success : function(json) {
					callback(json);
				}
			});
		},
		getCountUnread : function(sysNo, userId, type, callback) {
			var _data = config.params();
			_data['sysNo'] = sysNo;
			_data['userId'] = userId;
			_data['type'] = type;
			jQuery.ajax({
				url : config.address() + '/msgInfo/getCountUnread',
				data : _data,
				type : 'post',
				dataType : 'json',
				error : function(json){
					alert('异常');
				},
				success : function(json) {
					callback(json);
				}
			});
		},
		pageQueryUnread : function(page, size, sysNo, userId, groupId, type, callback) {
			var _data = config.params();
			_data['page'] = page;
			_data['size'] = size;
			_data['sysNo'] = sysNo;
			_data['userId'] = userId;
			_data['groupId'] = groupId;
			_data['type'] = type;
			jQuery.ajax({
				url : config.address() + '/msgInfo/pageQueryUnread',
				data : _data,
				type : 'post',
				dataType : 'json',
				error : function(json){
					alert('异常');
				},
				success : function(json) {
					callback(json);
				}
			});
		},
		pageQuery : function(page, size, sysNo, userId, groupId, isRead, type, callback) {
			var _data = config.params();
			_data['page'] = page;
			_data['size'] = size;
			_data['sysNo'] = sysNo;
			_data['userId'] = userId;
			_data['isRead'] = isRead;
			_data['groupId'] = groupId;
			_data['type'] = type;
			jQuery.ajax({
				url : config.address() + '/msgInfo/pageQuery',
				data : _data,
				type : 'post',
				dataType : 'json',
				error : function(json){
					alert('异常');
				},
				success : function(json) {
					callback(json);
				}
			});
		},
		findGroupUnread : function(sysNo, userId, type, callback) {
			var _data = config.params();
			_data['sysNo'] = sysNo;
			_data['userId'] = userId;
			_data['type'] = type;
			jQuery.ajax({
				url : config.address() + '/msgInfo/findGroupUnread',
				data : _data,
				type : 'post',
				dataType : 'json',
				error : function(json){
					alert('异常');
				},
				success : function(json) {
					callback(json);
				}
			});
		},
		getDtl : function(id, sysNo, userId, callback) {
			var _data = config.params();
			_data['id'] = id;
			_data['sysNo'] = sysNo;
			_data['userId'] = userId;
			jQuery.ajax({
				url : config.address() + '/msgInfo/getDtl',
				data : _data,
				type : 'post',
				dataType : 'json',
				error : function(json){
					alert('异常');
				},
				success : function(json) {
					callback(json);
				}
			});
		},
		del : function(id, callback) {
			var _data = config.params();
			_data['id'] = id;
			jQuery.ajax({
				url : config.address() + '/msgInfo/delete',
				data : _data,
				type : 'post',
				dataType : 'json',
				error : function(json){
					alert('异常');
				},
				success : function(json) {
					callback(json);
				}
			});
		},
		deleteRece : function(id, sysNo, userId, callback) {
			var _data = config.params();
			_data['id'] = id;
			_data['sysNo'] = sysNo;
			_data['userId'] = userId;
			jQuery.ajax({
				url : config.address() + '/msgInfo/deleteRece',
				data : _data,
				type : 'post',
				dataType : 'json',
				error : function(json){
					alert('异常');
				},
				success : function(json) {
					callback(json);
				}
			});
		},
		updateIsRead : function(id, sysNo, userId, isRead, callback) {
			var _data = config.params();
			_data['id'] = id;
			_data['sysNo'] = sysNo;
			_data['userId'] = userId;
			_data['isRead'] = isRead;
			jQuery.ajax({
				url : config.address() + '/msgInfo/updateIsRead',
				data : _data,
				type : 'post',
				dataType : 'json',
				error : function(json){
					alert('异常');
				},
				success : function(json) {
					callback(json);
				}
			});
		}
};


api.userGroupRule = {
		saveOrUpdate : function(sysNo, groupId, userId, status, emailStatus, smsStatus,
				recePhone, receEmail, callback) {
			var _data = config.params();
			_data['sysNo'] = sysNo;
			_data['groupId'] = groupId;
			_data['userId'] = userId;
			_data['status'] = status;
			_data['emailStatus'] = emailStatus;
			_data['smsStatus'] = smsStatus;
			_data['recePhone'] = recePhone;
			_data['receEmail'] = receEmail;
			jQuery.ajax({
				url : config.address() + '/userGroupRule/saveOrUpdate',
				data : _data,
				type : 'post',
				dataType : 'json',
				error : function(json){
					alert('异常');
				},
				success : function(json) {
					callback(json);
				}
			});
		},
		find : function(sysNo, userId, pid, callback) {
			var _data = config.params();
			_data['sysNo'] = sysNo;
			_data['userId'] = userId;
			_data['pid'] = pid;
			jQuery.ajax({
				url : config.address() + '/userGroupRule/find',
				data : _data,
				type : 'post',
				dataType : 'json',
				error : function(json){
					alert('异常');
				},
				success : function(json) {
					callback(json);
				}
			});
		}
}

package com.message.admin.send.service;

import java.util.Date;
import java.util.List;

import org.springframework.stereotype.Component;

import com.message.admin.send.pojo.SendSms;
import com.system.handle.model.ResponseFrame;

/**
 * send_sms的Service
 * @author autoCode
 * @date 2017-12-13 11:15:58
 * @version V1.0.0
 */
@Component
public interface SendSmsService {
	
	/**
	 * 保存待发送的短信
	 * @param msgId
	 * @param content
	 * @param phones	接收的手机号，多个用;分隔
	 * @param sendTime	发送的时间
	 * @return
	 */
	public ResponseFrame saveList(String msgId, String content, String phones, Date sendTime);
	
	/**
	 * 根据id获取对象
	 * @param id
	 * @return
	 */
	public SendSms get(String id);

	/**
	 * 分页获取对象
	 * @param sendSms
	 * @return
	 */
	public ResponseFrame pageQuery(SendSms sendSms);
	
	/**
	 * 根据id删除对象
	 * @param id
	 * @return
	 */
	public ResponseFrame delete(String id);

	public void updateWaitToIng(String servNo, Integer num);

	public List<SendSms> findIng(String servNo);

	public void updateStatus(String id, Integer status);
}
package com.message.admin.msg.service.impl;

import java.util.ArrayList;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import com.message.admin.msg.dao.MsgGroupDao;
import com.message.admin.msg.enums.MsgGroupType;
import com.message.admin.msg.pojo.MsgGroup;
import com.message.admin.msg.service.MsgGroupService;
import com.system.comm.model.ContrastBean;
import com.system.comm.model.Page;
import com.system.comm.utils.FrameBeanUtil;
import com.system.comm.utils.FrameStringUtil;
import com.system.comm.utils.FrameTimeUtil;
import com.system.handle.model.ResponseCode;
import com.system.handle.model.ResponseFrame;

/**
 * msg_group的Service
 * @author autoCode
 * @date 2017-12-04 17:13:15
 * @version V1.0.0
 */
@Component
public class MsgGroupServiceImpl implements MsgGroupService {

	@Autowired
	private MsgGroupDao msgGroupDao;

	@Override
	public void save(MsgGroup msgGroup) {
		if(FrameStringUtil.isEmpty(msgGroup.getName())) {
			msgGroup.setName(msgGroup.getId());
		}
		if(msgGroup.getType() == null) {
			msgGroup.setType(MsgGroupType.SYS.getCode());
		}
		if(FrameStringUtil.isEmpty(msgGroup.getPid())) {
			msgGroup.setPid("0");
		}
		msgGroup.setCreateTime(FrameTimeUtil.getTime());
		msgGroupDao.save(msgGroup);
	}

	@Override
	public ResponseFrame saveOrUpdate(MsgGroup msgGroup) {
		ResponseFrame frame = new ResponseFrame();
		MsgGroup org = get(msgGroup.getId(), msgGroup.getSysNo());
		if(org == null) {
			save(msgGroup);
		} else {
			List<ContrastBean> cdList = FrameBeanUtil.contrastDiff(org, msgGroup);
			for (ContrastBean cb : cdList) {
				if(FrameStringUtil.isEqualsArr(cb.getProperty(), "id", "sysNo", "type", "name", "pid")) {
					//存在不同的字段，则修改
					msgGroupDao.update(msgGroup);
				}
			}
		}
		frame.setCode(ResponseCode.SUCC.getCode());
		return frame;
	}

	@Override
	public MsgGroup get(String id, String sysNo) {
		if(FrameStringUtil.isEmpty(id) || "0".equals(id)) {
			return null;
		}
		return msgGroupDao.get(id, sysNo);
	}

	@Override
	public ResponseFrame pageQuery(MsgGroup msgGroup) {
		msgGroup.setDefPageSize();
		ResponseFrame frame = new ResponseFrame();
		int total = msgGroupDao.findMsgGroupCount(msgGroup);
		List<MsgGroup> data = null;
		if(total > 0) {
			data = msgGroupDao.findMsgGroup(msgGroup);
			for (MsgGroup mg : data) {
				mg.setTypeName(MsgGroupType.getText(mg.getType()));
			}
		}
		Page<MsgGroup> page = new Page<MsgGroup>(msgGroup.getPage(), msgGroup.getSize(), total, data);
		frame.setBody(page);
		frame.setCode(ResponseCode.SUCC.getCode());
		return frame;
	}

	@Override
	public ResponseFrame delete(String id) {
		ResponseFrame frame = new ResponseFrame();
		msgGroupDao.delete(id);
		frame.setCode(ResponseCode.SUCC.getCode());
		return frame;
	}

	@Override
	public List<MsgGroup> findUnread(String sysNo, String userId, Integer type) {
		List<MsgGroup> data = new ArrayList<MsgGroup>();
		List<MsgGroup> list = msgGroupDao.findUnread(sysNo, userId, type);
		for (MsgGroup mg : list) {
			MsgGroup group = get(mg.getId(), sysNo);
			group.setUnreadNum(mg.getUnreadNum());
			data.add(group);
		}
		return data;
	}

	@Override
	public String getName(String id, String sysNo) {
		MsgGroup obj = get(id, sysNo);
		return obj == null ? null : obj.getName();
	}

	@Override
	public List<MsgGroup> findBySysNo(String sysNo) {
		return msgGroupDao.findBySysNo(sysNo);
	}
}
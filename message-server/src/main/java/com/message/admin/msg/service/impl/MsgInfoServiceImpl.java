package com.message.admin.msg.service.impl;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;

import com.message.admin.msg.dao.MsgInfoDao;
import com.message.admin.msg.enums.MsgInfoStatus;
import com.message.admin.msg.enums.MsgInfoType;
import com.message.admin.msg.pojo.MsgGroup;
import com.message.admin.msg.pojo.MsgInfo;
import com.message.admin.msg.pojo.MsgRece;
import com.message.admin.msg.service.MsgGroupService;
import com.message.admin.msg.service.MsgInfoService;
import com.message.admin.msg.service.MsgReceService;
import com.message.admin.send.service.SendEmailService;
import com.message.admin.send.service.SendSmsService;
import com.message.admin.sys.enums.UserGroupRuleStatus;
import com.message.admin.sys.pojo.UserGroupRule;
import com.message.admin.sys.service.UserGroupRuleService;
import com.system.comm.enums.Boolean;
import com.system.comm.model.Page;
import com.system.comm.utils.FrameNoUtil;
import com.system.comm.utils.FrameStringUtil;
import com.system.comm.utils.FrameTimeUtil;
import com.system.handle.model.ResponseCode;
import com.system.handle.model.ResponseFrame;

/**
 * msg_info的Service
 * @author autoCode
 * @date 2017-12-04 17:13:15
 * @version V1.0.0
 */
@Component
public class MsgInfoServiceImpl implements MsgInfoService {

	@Autowired
	private MsgInfoDao msgInfoDao;
	@Autowired
	private MsgGroupService msgGroupService;
	@Autowired
	private MsgReceService msgReceService;
	@Autowired
	private UserGroupRuleService userGroupRuleService;
	@Autowired
	private SendEmailService sendEmailService;
	@Autowired
	private SendSmsService sendSmsService;

	@Override
	@Transactional(readOnly = false, propagation = Propagation.REQUIRES_NEW, rollbackFor = Exception.class)
	public ResponseFrame save(MsgInfo msgInfo) {
		ResponseFrame frame = new ResponseFrame();
		if(FrameStringUtil.isEmpty(msgInfo.getReceUserIds())) {
			frame.setCode(-2);
			frame.setMessage("接收人信息不能为空");
			return frame;
		}
		List<String> receUserIds = FrameStringUtil.toArray(msgInfo.getReceUserIds(), ";");
		if(receUserIds.size() == 0) {
			frame.setCode(-2);
			frame.setMessage("接收人信息不能为空");
			return frame;
		}
		if(FrameStringUtil.isEmpty(msgInfo.getTitle())) {
			frame.setCode(-2);
			frame.setMessage("消息标题不能为空");
			return frame;
		}
		MsgGroup group = msgGroupService.get(msgInfo.getGroupId(), msgInfo.getSysNo());
		if(group == null) {
			//创建分组
			group = new MsgGroup(msgInfo.getGroupId(), msgInfo.getSysNo());
			msgGroupService.save(group);
		}
		if(msgInfo.getStatus() == null) {
			msgInfo.setStatus(MsgInfoStatus.SUCC.getCode());
			msgInfo.setSendTime(FrameTimeUtil.getTime());
		}
		if(msgInfo.getType() == null) {
			msgInfo.setType(MsgInfoType.READ.getCode());
		}
		msgInfo.setId(FrameNoUtil.uuid());
		msgInfo.setCreateTime(FrameTimeUtil.getTime());
		msgInfoDao.save(msgInfo);

		//保存接收人信息
		msgReceService.saveList(msgInfo, receUserIds);

		if(msgInfo.getIsOpenCheck() == null || Boolean.FALSE.getCode() == msgInfo.getIsOpenCheck().intValue()) {
			String receContent = msgInfo.getReceContent();
			if(FrameStringUtil.isEmpty(receContent)) {
				receContent = msgInfo.getContent();
			}
			//发送短信
			if(FrameStringUtil.isNotEmpty(msgInfo.getRecePhones())) {
				sendSmsService.saveList(msgInfo.getId(), receContent, msgInfo.getRecePhones(), msgInfo.getSendTime());
			}
			//发送邮件
			if(FrameStringUtil.isNotEmpty(msgInfo.getReceEmails())) {
				sendEmailService.saveList(msgInfo.getId(), msgInfo.getTitle(), receContent, msgInfo.getReceEmails(), msgInfo.getSendTime(), msgInfo.getReceEmailFiles());
			}
		} else {
			//判断第一个接收人是否打开接收消息的设置
			String receUserId = receUserIds.get(0);
			UserGroupRule ugr = userGroupRuleService.getByGsu(msgInfo.getGroupId(), msgInfo.getSysNo(), receUserId);
			if(ugr != null && UserGroupRuleStatus.OPEN.getCode() == ugr.getStatus().intValue()) {
				String receContent = msgInfo.getReceContent();
				if(FrameStringUtil.isEmpty(receContent)) {
					receContent = msgInfo.getContent();
				}
				//发送短信
				if(UserGroupRuleStatus.OPEN.getCode() == ugr.getSmsStatus().intValue() && FrameStringUtil.isNotEmpty(msgInfo.getRecePhones())) {
					sendSmsService.saveList(msgInfo.getId(), receContent, msgInfo.getRecePhones(), msgInfo.getSendTime());
				}
				//发送邮件
				if(UserGroupRuleStatus.OPEN.getCode() == ugr.getEmailStatus().intValue() && FrameStringUtil.isNotEmpty(msgInfo.getReceEmails())) {
					sendEmailService.saveList(msgInfo.getId(), msgInfo.getTitle(), receContent, msgInfo.getReceEmails(), msgInfo.getSendTime(), msgInfo.getReceEmailFiles());
				}
			}
		}
		frame.setCode(ResponseCode.SUCC.getCode());
		return frame;
	}

	@Override
	public MsgInfo get(String id) {
		return msgInfoDao.get(id);
	}

	@Override
	public ResponseFrame deleteRece(String id, String sysNo, String userId) {
		ResponseFrame frame = new ResponseFrame();
		MsgInfo msgInfo = get(id);
		if(msgInfo == null) {
			frame.setCode(-2);
			frame.setMessage("不存在该消息");
			return frame;
		}
		MsgRece rece = msgReceService.getByMsgIdReceUserId(msgInfo.getId(), userId);
		if(rece == null) {
			frame.setCode(-3);
			frame.setMessage("您不存在阅读该消息记录的权限");
			return frame;
		}
		msgReceService.delete(rece.getId());
		frame.setCode(ResponseCode.SUCC.getCode());
		return frame;
	}

	@Override
	public ResponseFrame pageQuery(MsgInfo msgInfo) {
		msgInfo.setDefPageSize();
		ResponseFrame frame = new ResponseFrame();
		if(FrameStringUtil.isNotEmpty(msgInfo.getGroupIds())) {
			List<String> groupIdList = FrameStringUtil.toArray(msgInfo.getGroupIds(), ";");
			if(groupIdList.size() > 0) {
				msgInfo.setGroupIdList(groupIdList);
			}
		}
		int total = msgInfoDao.findMsgInfoCount(msgInfo);
		List<MsgInfo> data = null;
		if(total > 0) {
			data = msgInfoDao.findMsgInfo(msgInfo);
			for (MsgInfo mi : data) {
				mi.setTypeName(MsgInfoType.getText(mi.getType()));
			}
		}
		Page<MsgInfo> page = new Page<MsgInfo>(msgInfo.getPage(), msgInfo.getSize(), total, data);
		frame.setBody(page);
		frame.setCode(ResponseCode.SUCC.getCode());
		return frame;
	}

	@Override
	public ResponseFrame getDtl(String id, String sysNo, String userId) {
		ResponseFrame frame = new ResponseFrame();
		MsgInfo msgInfo = get(id);
		if(msgInfo == null) {
			frame.setCode(-2);
			frame.setMessage("不存在该消息记录");
			return frame;
		}
		MsgRece rece = msgReceService.getByMsgIdReceUserId(msgInfo.getId(), userId);
		if(rece == null) {
			frame.setCode(-3);
			frame.setMessage("您不存在阅读该消息记录的权限");
			return frame;
		}
		//修改状态为已读
		if(MsgInfoType.READ.getCode() == msgInfo.getType().intValue()) {
			msgReceService.updateIsRead(id, sysNo, userId, Boolean.TRUE.getCode());
		}
		frame.setBody(msgInfo);
		frame.setSucc();
		return frame;
	}

	@Override
	public ResponseFrame updateIsRead(String id, String sysNo, String userId,
			Integer isRead) {
		ResponseFrame frame = new ResponseFrame();
		MsgInfo msgInfo = get(id);
		if(msgInfo == null) {
			frame.setCode(-2);
			frame.setMessage("不存在该消息记录");
			return frame;
		}
		MsgRece rece = msgReceService.getByMsgIdReceUserId(msgInfo.getId(), userId);
		if(rece == null) {
			frame.setCode(-3);
			frame.setMessage("您不存在阅读该消息记录的权限");
			return frame;
		}
		//修改状态为已读
		msgReceService.updateIsRead(id, sysNo, userId, isRead);
		frame.setSucc();
		return frame;
	}

	@Override
	public List<MsgInfo> findByExt(String sysNo, String ext1, String ext2,
			String ext3) {
		return msgInfoDao.findByExt(sysNo, ext1, ext2, ext3);
	}

	@Override
	public ResponseFrame delete(String id) {
		ResponseFrame frame = new ResponseFrame();
		msgInfoDao.delete(id);
		msgReceService.deleteByMsgId(id);
		frame.setSucc();
		return frame;
	}
}
package com.message.ui.sys.dao;

import java.util.List;

import org.apache.ibatis.annotations.Param;

import com.message.ui.sys.pojo.AdminUser;

/**
 * admin_user的Dao
 * @author autoCode
 * @date 2017-12-27 15:24:05
 * @version V1.0.0
 */
public interface AdminUserDao {

	public abstract void save(AdminUser adminUser);

	public abstract void update(AdminUser adminUser);

	public abstract void delete(@Param("id")String id);

	public abstract AdminUser get(@Param("id")String id);

	public abstract List<AdminUser> findAdminUser(AdminUser adminUser);
	
	public abstract int findAdminUserCount(AdminUser adminUser);

	public abstract AdminUser getByUsername(@Param("username")String username);
}